<?php
class cash_model{
    private $db_conex;
    private $cash;
 
    public function __construct(){
        $this->db_conex=Database::get();
        $this->cash=array();
    }
    public function get_cash($user_id){
        $sql_query=$this->db_conex->query("select * from cash where user_id = {$user_id};");
        while($rows=$sql_query->fetch(\PDO::FETCH_ASSOC)){
            $this->cash[]=$rows;
        }
        return $this->cash;
    }
}
?>
