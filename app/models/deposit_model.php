<?php
class deposit_model{
    private $db_conex;

    public function __construct(){
        $this->db_conex=Database::get();
    }
    public function do_deposit($amount, $user_id){
        $fecha = new DateTime();
        $fecha = $fecha->getTimestamp();
        $sql_query=$this->db_conex->prepare(
            "UPDATE public.cash
            SET amount=(amount+:amount::money)
            WHERE user_id=:user_id;"
        )->execute([
            'amount' => $amount,
            'user_id' => $user_id
        ]);
    }
}
?>
