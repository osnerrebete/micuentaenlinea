<!DOCTYPE html>
<head>
	<title>Iniciar Sesión | MiCuentaEnLinea</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h2>Depósito</h2>
		</div>
			<div class="row">
			<form name="signin" action="../app/controllers/signin_controller.php" method="POST" >
				<div class="form-group">
					<label for="rut">Cuenta a realizar el depósito:</label>
					<input type="text" name="rut" id="rut" class="form-control" required/>
				</div>
				<div class="form-group">
					<label for="amount">Monto:</label>
					<input type="number" name="amount" min="0" value="0" step=".01" required>
				</div>
				<input type="submit" class="btn btn-primary" />
			</form>
		</div>
	</div>
</body>
</html>