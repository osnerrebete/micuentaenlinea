<!DOCTYPE html>
<head>
	<title>Iniciar Sesión | MiCuentaEnLinea</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script
	src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="text-center">
			<h1>Mi cuenta</h1>	
		</div>
		<div class="row">
			<div class="col-md-2 nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="../app/controllers/cash_controller.php" aria-selected="true">Mi cuenta</a>
				<a class="nav-link" href="/../app/controllers/deposit_controller.php">Depósitos</a>
				<a class="nav-link" href="/../app/controllers/transfer_controller.php">Transferencias</a>
				<a class="nav-link" href="/../app/controllers/history_controller.php">Histórico</a>
				<a class="nav-link" href="/../app/controllers/signout_controller.php">Salir</a>
			</div>
			<div class="col-md-10 tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<div class="alert alert-success" role="alert">
						<h4 class="alert-heading">¡Bienvenido/a!</h4>
						<p>Aquí podrás realizar tus operaciones bancarias.</p>
					</div>
					<div class="card" style="width: 18rem;">
						<div class="card-body">
							<h5 class="card-title">Saldo actual</h5>
							<h6 class="card-subtitle mb-2 text-muted">CUENTA RUT</h6>
							<p class="card-text">Su saldo a la fecha es: <?php echo $amount[0]['amount']; ?></p>
							<a href="#" class="card-link">Última actualización el: <?php echo date_format(date_create($amount[0]['updated_at']), 'Y-m-d H:i:s'); ?></a>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>