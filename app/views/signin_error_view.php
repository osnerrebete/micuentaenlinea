<!DOCTYPE html>
<head>
	<title>Iniciar Sesión | MiCuentaEnLinea</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h2>Iniciar Sesión</h2>
		</div>
		<div class="row">
			<div class="alert alert-danger" role="alert">
				¡El usuario/clave no existen! Por favor intente nuevamente...
			</div>
		</div>
		<div class="row">
			<form name="signin" action="../app/controllers/signin_controller.php" method="POST" >
				<div class="form-group">
					<label for="rut">RUT:</label>
					<input type="text" name="rut" id="rut" class="form-control"/>
				</div>
				<div class="form-group">
					<label for="password">Clave:</label>
					<input type="password" name="password" id="password" class="form-control" />
				</div>
				<input type="submit" class="btn btn-primary" />
			</form>
		</div>
	</div>
</body>
</html>