<?php
//model
require_once(__DIR__."/session_controller.php");
require_once(__DIR__."/../../config/database.php");
require_once(__DIR__."/../models/deposit_model.php");

$deposit_model = new deposit_model();
$result = $deposit_model->do_deposit($_POST['amount'], $_SESSION["user_id"]);

//view
require_once(__DIR__."/../views/deposit_view.php");

?>