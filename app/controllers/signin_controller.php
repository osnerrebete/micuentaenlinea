<?php
session_start();
//model
require_once(__DIR__."/../../config/database.php");
require_once(__DIR__."/../models/users_model.php");

$users_model = new users_model();


//if the user is OK
$user = $users_model->signin_user($_POST['rut'],$_POST['password']);
if($user){
	$_SESSION["user_id"]= $user[0]['id'];
	require_once(__DIR__."/cash_controller.php");
	exit;
}else{
	require_once(__DIR__."/../views/signin_error_view.php");
	exit;
}
//view
require_once(__DIR__."/../views/signin_view.php");

?>