<?php
//model
require_once(__DIR__."/session_controller.php");
require_once(__DIR__."/../../config/database.php");
require_once(__DIR__."/../models/cash_model.php");

$cash_model = new cash_model();
$amount = $cash_model->get_cash($_SESSION["user_id"]);
//view
require_once(__DIR__."/../views/cash_view.php");

?>