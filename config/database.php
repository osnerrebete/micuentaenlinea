<?php
class Database {
	private static $db_conex;

	public function connect() {
		$db_params = array(
			'host' => 'localhost', 
			'port' => '5432', 
			'dbname' => 'postgres',
			'user' => 'postgres', 
			'password' => '20483015o'
		);
		$params = sprintf("pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s", 
			$db_params['host'], 
			$db_params['port'], 
			$db_params['dbname'], 
			$db_params['user'], 
			$db_params['password']);

		$conex = new \PDO($params);
		$conex->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		return $conex;
	}

	public static function get() {
		if (null === $db_conex) {
			try {
				$db_conex =  self::connect();
				//echo 'Conexión satisfactoria.';
			} catch (\PDOException $e) {
				echo $e->getMessage();
				exit;
			}
		}
		return $db_conex;
	}
}